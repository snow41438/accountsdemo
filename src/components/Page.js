import React, { Component } from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { Slider, InputNumber, Row, Col, Button } from "antd";
// const axios = require("axios");

import { searchData } from "../redux/actions";
const Wrapper = styled.div`
  height: 100% !important;
  position: absolute;
  width: 100% !important;
  display: flex;
`;
const Container = styled.div`
  display: flex;
  width: 100% !important;
  margin: 30px 30px;
  @media screen and (max-width: 360px) {
    width: 100%;
    flex-direction: column;
    margin: unset;
  }
`;
const url = "https://ftl-frontend-test.herokuapp.com/interest";
var a = "";
const InnerContainer = styled.div`
  /* padding: 2px; */
  @media screen and (max-width: 360px) {
    /* width: 100%; */
    flex-direction: column;
  }
`;

const CacheContainer = styled.div`
  flex: 1;
  height: 85%;
  /* margin: 30px 30px 0 0; */
  padding: 10px 10px 20px 10px;
  border: 1px solid #bfbfbf;
  background-color: white;
  box-shadow: 10px 10px 5px #aaaaaa;
  @media screen and (max-width: 360px) {
    margin-top: 18px;
  }
  > div {
    height: 90%;
    overflow: auto;
    @media screen and (max-width: 360px) {
      height: unset;
    }
  }
  > span {
    background-color: red;
  }
`;
const ResultText = styled.h2`
  background-color: #64b5f6 !important;
  color: #fff !important;
  border-radius: 0.125rem;
  font-size: 23px;
  text-align: center;
  font-weight: 600;
  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
  @media screen and (max-width: 360px) {
    height: unset;
    font-size: 18px;
    /* text-align: center; */
    font-weight: 500;
  }
`;
const ResultTextamount = styled.h2`
  margin-top: 20px;
  > div {
    color: #0d47a1;
    font-weight: 950;
    font-size: 40px;
    @media screen and (max-width: 360px) {
      text-align: center;
    }
  }
`;
const ResultBox = styled.div`
  flex: 1;
`;
const SliderContainer = styled.div`
  /* padding: 80px 20px 0 100px; */
  flex: 3;
  display: flex;
  padding: 30px;
  flex-direction: column;
  /* flex-direction:c */
  /* justify-content: space-between; */
  @media screen and (max-width: 360px) {
    padding: unset;
  }
  > div {
    display: flex;
    > div {
      margin-right: 70px;
      @media screen and (max-width: 360px) {
        margin-right: unset;
      }
    }
  }
  > button {
    width: 150px;
    margin: auto;
    margin-top: 60px;
    justify-self: center;
    :hover {
      background: red;
    }
    @media screen and (max-width: 360px) {
      margin-top: 20px;
    }
  }
`;
const CustomizedSlider = styled(Slider)`
  /* > div[class="ant-slider-handle"] {
    width: 30px;
    height: 30px;

    &:hover {
      width: 30px;
      height: 30px;
    }

  /* } */
  > div:nth-child(1) {
    height: 8px;
  }
  > div:nth-child(2) {
    height: 8px;
  }
  > div:nth-child(3) {
    height: 8px;
  }
  > div:nth-child(4) {
    width: 20px;
    height: 20px;
    margin-top: -8px;
    /* margin-left: -13px; */
  }
`;
const HeadText = styled.h2`
  /* color: #000; */
  font-size: 30px;
  font-weight: 600;
  margin-left: 10px;
  color: #2196f3 !important;
`;
const ResultCOntainer = styled.div`
  margin-top: 60px;
  width: 100%;
  @media screen and (max-width: 360px) {
    flex-direction: column;
  }
`;
const RescentCard = styled.div`
  border: 1px solid #bfbfbf;
  margin: 10px;
  border-radius: 5px;
  padding: 10px;
  cursor: pointer;
  > div {
    text-align: center;
    /* justify-content: space-between; */
    display: flex;
    > b {
      color: #0d47a1;
      font-size: 18px;
    }
    > span {
      font-size: 18px;
      font-weight: 550;
      margin-left: 15%;
    }
  }
  > div:nth-child(3) {
    justify-content: flex-end;
    text-align: right;
    font-size: 12px;
    background-color: bisque;
    color: cadetblue;
  }
`;
const NewSlider = styled(Slider)`
  width: 350px;
  margin-top: 40px;
  > div:nth-child(1) {
    height: 3px;
    background: #595959;
  }
  > div:nth-child(2) {
    height: 3px;
    background: #595959;
  }
  > div:nth-child(3) {
    height: 3px;
    background: #595959;
  }
  > div:nth-child(4) {
    width: 15px;
    height: 15px;
    margin-top: -6px;
    background-color: red;
    /* margin-left: -13px; */
  }
`;

class Page extends Component {
  state = {
    Amount: 2200,
    duration: 10,
    cachedData: []
  };

  componentDidMount = () => {
    // localStorage.removeItem("ArrayData5");
    searchData(this.state);
    if (localStorage.ArrayData5) {
      var cache = JSON.parse(localStorage.ArrayData5);

      this.setState({ cachedData: cache });
    }
  };
  onChangeAmount = value => {
    this.setState({
      Amount: value
    });
    searchData(this.state);
  };

  cardHandler = (e, index) => {
    console.log("target ", index);
    var value = this.state.cachedData[index];
    this.setState({
      Amount: value.amount,
      duration: value.duration
    });
    searchData(this.state);
  };
  onChangeDuration = value => {
    this.setState({
      duration: value
    });
    searchData(this.state);
  };

  saveHandler = () => {
    var data = localStorage.ArrayData5;
    var d = Date().split(" ");
    var curdate = d[2] + " " + d[1] + " " + d[3];
    var recentData = {
      amount: this.state.Amount,
      duration: this.state.duration,
      date: curdate
    };
    if (data) {
      var storagedata = JSON.parse(data);

      console.log("storagedata before", storagedata);

      storagedata.push(recentData);
      console.log("storagedata after", storagedata);

      localStorage.ArrayData5 = JSON.stringify(storagedata);
      this.setState({ cachedData: storagedata });
    } else {
      // console.log("in else");
      var arr = [];
      // var recentData = {
      //   amount: this.state.Amount,
      //   duration: this.state.duration,
      //   date: curdate
      // };
      arr.push(recentData);
      localStorage.ArrayData5 = JSON.stringify(arr);
    }
  };

  render() {
    const { Amount, duration } = this.state;
    console.log("state ", this.state);
    const { searchResult } = this.props;

    const result = (this.state.cachedData || []).map((d, i) => {
      console.log("d ", i);
      return (
        <RescentCard onClick={d => this.cardHandler(d, i)} value={i}>
          <div>
            <b>Loan Amount : </b>
            <span>{d.amount}</span>
          </div>
          <div>
            <b>Loan Duration : </b>
            <span>{d.duration}</span>
          </div>
          <div>{d.date}</div>
        </RescentCard>
      );
    });
    // console.log("result ", this.state);
    return (
      <Wrapper>
        <Container>
          <SliderContainer>
            <InnerContainer>
              <div>
                <HeadText>Loan Amount</HeadText>
                <Row>
                  <Col span={12}>
                    <NewSlider
                      min={500}
                      max={5000}
                      onChange={this.onChangeAmount}
                      value={typeof Amount === "number" ? Amount : 0}
                    />
                  </Col>
                  <Col span={4} style={{ display: "none" }}>
                    <InputNumber
                      min={500}
                      max={5000}
                      style={{ marginLeft: 16 }}
                      value={Amount}
                      onChange={this.onChangeAmount}
                    />
                  </Col>
                </Row>
              </div>
              <div>
                <HeadText> Loan Duration</HeadText>
                <Row>
                  <Col span={12}>
                    <NewSlider
                      min={6}
                      max={24}
                      onChange={this.onChangeDuration}
                      value={typeof duration === "number" ? duration : 0}
                    />
                  </Col>
                  <Col span={4} style={{ display: "none" }}>
                    <InputNumber
                      min={6}
                      max={24}
                      style={{ marginLeft: 16 }}
                      value={duration}
                      onChange={this.onChangeDuration}
                    />
                  </Col>
                </Row>
              </div>
            </InnerContainer>
            <ResultCOntainer>
              <ResultBox>
                <ResultText>Principal</ResultText>
                <ResultTextamount>
                  {/* {searchResult.principal} USD */}
                  {searchResult ? (
                    <div>{searchResult.principal.amount} USD</div>
                  ) : (
                    ""
                  )}
                </ResultTextamount>
              </ResultBox>
              <ResultBox>
                <ResultText>Monthly Payment</ResultText>
                <ResultTextamount>
                  {/* {searchResult.monthlyPayment} */}
                  {searchResult ? (
                    <div>{searchResult.monthlyPayment.amount} USD</div>
                  ) : (
                    ""
                  )}
                </ResultTextamount>
              </ResultBox>
              <ResultBox>
                <ResultText>Num of Payments</ResultText>
                <ResultTextamount>
                  {/* {searchResult.numPayments} */}
                  {searchResult ? (
                    <div>{searchResult.numPayments} times</div>
                  ) : (
                    ""
                  )}
                </ResultTextamount>
              </ResultBox>
            </ResultCOntainer>

            <Button type="primary" onClick={this.saveHandler} ghost>
              Save
            </Button>
          </SliderContainer>
          <CacheContainer>
            <span>
              <h1>Recent Searches</h1>
            </span>
            <div>{result}</div>
          </CacheContainer>
        </Container>
      </Wrapper>
    );
  }
}

const mapstatetoprops = state => {
  console.log("mapstatetoprops ", state);
  return {
    ...state
  };
};

export default connect(mapstatetoprops)(Page);
