import React, { Component } from "react";
import styled from "styled-components";
import Header from "./Header";
import Page from "./Page";

const Wrapper = styled.div`
  /* height: 80px;
  width: 100%; */
  /* background: #595959; */
`;
class HomePage extends Component {
  state = {};
  render() {
    return (
      <Wrapper>
        <Header />
        <Page />
      </Wrapper>
    );
  }
}

export default HomePage;
