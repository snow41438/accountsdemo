import React, { Component } from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  height: 80px;
  width: 100%;
  /* box-shadow: rgba(0, 0, 0, 0.4) 0px -2px 10px; */
  background: #595959;
  color: #00b8e6;
  font-size: 25px;
  > b {
    font-weight: 1000;
  }

  padding: 15px 0 0 15px;
`;
class Header extends Component {
  state = {};
  render() {
    return (
      <Wrapper>
        <b>Calculate Interest </b>
      </Wrapper>
    );
  }
}

export default Header;
