import React from "react";
import logo from "./logo.svg";
import "./App.css";
import HomePage from "./components/index";
import { Provider } from "react-redux";
import store from "./redux/store";
function App() {
  return (
    <Provider store={store}>
      <div>
        <HomePage />
      </div>
    </Provider>
  );
}

export default App;
// https://calculateintrest.herokuapp.com
