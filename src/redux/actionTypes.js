import keyMirror from "keymirror";

export default keyMirror({
  SEARCH_RESULT: null
});
