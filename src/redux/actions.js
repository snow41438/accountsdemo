import ActionTypes from "./actionTypes";
import axios from "axios";
import store from "./store";
const key_url = "https://ftl-frontend-test.herokuapp.com/interest";
const query = "?amount=600&numMonths=12";

export const searchData = data => {
  console.log("data ", data);
  axios
    .get(key_url + "?amount=" + data.Amount + "&numMonths=" + data.duration)
    .then(response => {
      const result = response.data;
      const action = {
        type: ActionTypes.SEARCH_RESULT,
        payload: result
      };
      store.dispatch(action);
    });
};
